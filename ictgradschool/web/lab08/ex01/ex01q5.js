"use strict";



// Provided variable
var theString = "Hello World!";

// Variable you'll be modifying
var reversed = "";
console.log(theString.length);
// TODO Your answer here.
var i;
for (i = theString.length -1; i >= 0 ; i--) {
    reversed += theString[i];
}

// Printing the answer.
console.log("\"" + theString + "\", reversed, is: \"" + reversed + "\".");