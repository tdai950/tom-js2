"use strict";

function factorial(number) {
    if (number == 1) {
        return 1;
    }
    var tempnumber;
    var factorialResult = number;
    for (tempNumber=number; tempNumber > 1; tempNumber--) {
        factorialResult *= (tempNumber - 1);
    }
    return factorialResult;
}
var i;
for (i = 1; i <= 10; i++) {
    console.log("The factorial of " + i + " = " + factorial(i));
}